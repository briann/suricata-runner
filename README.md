This project contains what's necessary to build a Docker image with Suricata IDS
and rsyslog installed. To enable remote logging the rsyslog configuration must
be replaced what an appropriate client config that includes a logging destination.

The IDS config includes emerging threat rules under `/etc/suricata/rules/emerging-*`.

By default Suricata will listen on the `eth0` interface.

This image must be run with `docker run --net=host` so that Suricata has access
to the primary network interface.
